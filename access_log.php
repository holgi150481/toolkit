<?php
/**
 * Apache AccessLog aufbereiten und als PHP Array ausgeben
 * 
 * Es wird das Apache Access Log aufbereitet und als Assioziatives Array wieder an PHP zurück gegeben. 
 * @package Toolkit
 */

class AccessLog {

	/**
	 * Das importierte Access Log
	 * @var array AccessLog
	 */
	private $accessLog;
	
	/**
	 * Aufbereitetes Log
	 * @var arrray Aufbereitet
	 */
	private $accessArray = array(); 	

	const PATTERN_IP 			= '/(\d{1,3})[\.](\d{1,3})[\.](\d{1,3})[\.](\d{1,3})/';
	const PATTERN_TYPE 			= '/(POST|GET)/';
	const PATTERN_FILE 			= '/(POST|GET\s)(.*)(HTTP)/';
	const PATTERN_TIME 			= '/(\[)(.*)(\])/';
	const PATTERN_STATUS_SIZE 	= '/(\d{3}\s)(\d+)/';
	const PATTERN_PROTOCOL		= '/(HTTP)(.+)(\"\s\d{1,3})/';

	/**
	 * Konstruktor
	 * 
	 * Access Log aus Verzeichnis laden
	 * @param string $file Log Datei
	 */
	public function __construct($file) {
		if (!file_exists($file)) {
			die('Log konnte nicht geladen werden.');
		} else {
			$this->accessLog = $handle = file($file);	
		}
		return $this;		
	}

	/**
	 * Array zusammenbauen	 
	 */
	private function buildArray() {
		foreach ($this->accessLog as $line) {
			$time = strtotime($this->regEx(self::PATTERN_TIME, $line, 2));
			$this->accessArray[] = array(
				'ip' 		=> $this->regEx(self::PATTERN_IP, 	$line, 0),
				'type' 		=> $this->regEx(self::PATTERN_TYPE, $line, 0),
				'status'	=> $this->regEx(self::PATTERN_STATUS_SIZE, $line, 1),
				'file' 		=> $this->regEx(self::PATTERN_FILE, $line, 2),
				'size' 		=> $this->regEx(self::PATTERN_STATUS_SIZE, $line, 2),
				'protocol' 	=> 'HTTP' . $this->regEx(self::PATTERN_PROTOCOL, $line, 2),
				'date' 		=> date('d.m.Y', $time),
				'time' 		=> date('H:m:i', $time)
			);
		}
	} 
	
	/**
	 * Regex
	 * @param string $pattern Suchmuster	 
	 * @param string $var zu durchsuchender String
	 * @param integer $num Rückgabewert aus preq_match Array
	 * @return string Fundstück
	 */
	private function regEx($pattern, $var, $num) {
		preg_match($pattern, $var, $match);
		return $match[$num];
	}

	/**
	 * Gibt das Log als Array wieder aus
	 * @return array AccessLog
	 */
	public function getLog() {
		$this->buildArray();	
		$result = array_reverse($this->accessArray, true);	
		return $result;
	}

}