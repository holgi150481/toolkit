<?php
/**
 * Template System
 * 
 * Ein Einfaches Template System.
 * Texte werden durch str_replace ersetzt. 
 * Es können verschiedene Sprachen genutzt werden
 * @package Toolkit
 */

class Template {

	/**
	 * Seiten Inhalt
	 * @var array Seiteninhalt
	 */		
	private $content = array();
	
	/**
	 * Kopf Inhalt
	 * @var array Kopf
	 */
	private $head = array();
	
	/**
	 * Body Inhalt
	 * @var array Body
	 */
	private $body = array();
	
	/**
	 * Footer Inhalt
	 * @var array Footer
	 */
	private $foot = array();
	
	/**
	 * Template Name
	 * @var string Template Name
	 */
	private $template;	
	
	/** 
	 * Seiten
	 * @var array Seiten
	 */
	private $pages = array();
	
	/**
	 * Manuell gesetzte Ersetzungen
	 * @var array Replace Variable
	 */
	private $replace_array = array();
	
	/**
	 * Aus Datenbank gelesene Ersetzungen
	 * @var array Replace Variable
	 */
	private $db_array = array();
	
	/**
	 * Datenkbank Objekt
	 * @var object Datenbank
	 */	
	private $db;
	
	/**
	 * Sprach
	 * @var string Sprache
	 */
	private $lang = 'de';

	/**
	 * Konstruktor
	 * 
	 * Datenban Verbindung aufbauen und Template setzen
	 * @param string $template Template Name
	 */
	public function __construct($template) {
		$this->db = SQL_PDO::getInstance();
		$this->template = $template;		
	}
	
	/**
	 * Ersetzungen aus Datenbank laden
	 */
	private function loadStrings() {
		if ($this->db) {
			$sql = 'SELECT DISTINCT tpl_key, tpl_' . $this->lang . ' FROM ' . TABLE_TEMPLATE;
			$result = $this->db->query($sql);	
		
			foreach ($result as $key => $value) {
				$this->db_array[$value['tpl_key']] = $value['tpl_' . $this->lang];
			}
		}
	}
	
	/** 
	 * Footer und Header laden
	 */
	private function loadPageFiles() {
		$this->head = file('templates/' . $this->template . '/header.html');
		$this->foot = file('templates/' . $this->template . '/footer.html');		
	}
	
	/** 
	 * Ersetzungen durchführen
	 */
	private function replaceData() {
		foreach ($this->replace_array as $key => $value) {
			$this->content = str_replace('{$' . $key . '}', $value, $this->content);
		}
		foreach ($this->db_array as $key => $value) {
			$this->content = str_replace('{$' . $key . '}', $value, $this->content);
		}		
	}
	
	/**
	 * Seite zusammenbauen
	 */	
	private function buildPage() {
		$this->loadPageFiles();
		foreach ($this->pages as $value) {
			if (file_exists('templates/' . $this->template . '/' . $value . '.css')) {
				$this->replace_array['style'] = '<link href="templates/tpl1/' . $value . '.css" rel="stylesheet" type="text/css">' . "\n";
			} else {
				$this->replace_array['style'] = '';
			}
			if (file_exists('templates/' . $this->template . '/' . $value . '.js')) {
				$js = file_get_contents('templates/tpl1/' . $value . '.js');
				$this->replace_array['script'] = "<script>\n" . $js . "\n</script>";
			} else {
				$this->replace_array['script'] = '';
			}
		}
		$this->loadStrings();
		$this->content = array_merge($this->head, $this->body, $this->foot);
		$this->replaceData();		
	}
	
	/**
	 * Haupt Content laden
	 * @param string $file Dateinme
	 */
	public function addContent($file) {
		$data = file('templates/' . $this->template . '/' . $file . '.html');
		foreach ($data as $value) {
			array_push($this->body, $value);
			array_push($this->pages, $file);
		}		
	}
	
	/**
	 * Manuelle Ersetzung einfügen
	 * @param string $key Platzhalter
	 * @param string $value Ersetzung
	 */
	public function setVar($key, $value) {
		$this->replace_array[$key] = $value;
	}
	
	/**
	 * Sprache setzen
	 * @param strint $lang Sprrache
	 */
	public function setLang($lang) {
		$this->lang = $lang;
	}
	
	/**
	 * Seiteninhalt ausgeben
	 */
	public function output() {		
		$this->buildPage();
		foreach ($this->content as $value) {
			echo $value;	
		}
	}
}