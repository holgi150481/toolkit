<?php
/**
 * Loader
 * 
 * Alle Klassen aus dem Toolkit für das PHP script verfügbar machen
 */

class Loader {
	
	/**
	 * Klassen laden
	 */
	public static function load() {		
		include 'sql/sql_pdo.php';		
		include 'sql/sql_exception.php';		
		include 'settings.php';
		include 'access_log.php';
		include 'template.php';
		include 'tracking.php';
		include 'authentification.php';
		
		require_once('/var/www/include/sql_formatter.php');
	} 
	
}