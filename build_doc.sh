#!/bin/bash
rm -rf main/doc
phpdoc --directory /var/www/toolkit  --target /var/www/subdomain/doc --title "howdynet.de Toolkit" --parseprivate --sourcecode

### Parameter ###
# [-t|--target[="..."]]
# [-f|--filename[="..."]]
# [-d|--directory[="..."]]
# [--encoding[="..."]]
# [-e|--extensions[="..."]]
# [-i|--ignore[="..."]]
# [--ignore-tags[="..."]]
# [--hidden]
# [--ignore-symlinks]
# [-m|--markers[="..."]]
# [--title[="..."]]
# [--force]
# [--validate]
# [--visibility[="..."]]
# [--defaultpackagename[="..."]]
# [--sourcecode]
# [-p|--progressbar]
# [--template[="..."]]
# [--parseprivate]
# [-c|--config[="..."]]

### Templates ###
# abstract
# checkstyle
# new-black
# old-ocean
# responsive
# zend
