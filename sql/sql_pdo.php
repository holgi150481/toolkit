<?php 
/**
 * Erweiterung der PHP PDO Klasse
 * 
 * Stellt handlichere Methoden für den Datenbank zugriff zur verfügung
 * @package SQL
 */

class SQL_PDO extends PDO {
		
	/**
	 * Gesamtdauer DB Zugriff
	 * @var float Zeit
	 */
	private $timer;
	
	/**
	 * Anzahl zugriffe
	 * @var int Zugriffe
	 */
	private $counter;
	
	/**
	 * Array mit allen ausgeführten Queries inkl. Zusatzinformationen
	 * @var array Queries
	 */	
	private $queries = array();
	
	/**
	 * Connection String
	 * @var string connection
	 */
	private $connection;
	
	/**
	 * Benutzername
	 * @var string user
	 */
	private $user;
	
	/**
	 * Passwort
	 * @var string passwort
	 */
	private $pass; 
	
	/**
	 * Array f�r Fehlermeldungen
	 * @var array Fehlermeldungen
	 */
	private $errors = array();
	
	/**
	 * SQL Befehle
	 * @var array sqlCommants
	 */
	private $sqlCommands = array(
		'NOW()'
	);
	
	/** 
	 * PDO Einstellungen
	 * @var array options
	 */
	private $options = array (
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
	);
	
	/**
	 * Singleton Pattern 
	 * @var object instance
	 */
	private static $instance = null;
	
	/**
	 * Singleton Pattern um Object nachzuladen
	 * @return object $instance Datenbank Objekt
	 */
	public static function getInstance() {
		if (self::$instance == null) {
			self::$instance = new SQL_PDO;
		}		
		return self::$instance;
	}
	
	
	/** 
	 * Kontruktor
	 * 
	 * Datenbank Verbindung aufbauen und @see $instance setzen
	 * @param string $host Hostname
	 * @param string $user Bentzername
	 * @param string $pass Passwort
	 * @param string $name Datenankname
	 * @throws SQLException Wenn Verbindung fehlgeschlagen
	 */
	public function __construct($host, $user, $pass, $name) {
		self::$instance = $this;
		$this->connection = 'mysql:dbname=' . $name . '; host=' . $host . '; port=3333';
		$this->user = $user;
		$this->pass = $pass;
		try {
  			parent::__construct($this->connection, $this->user, $this->pass, $this->options);
		} 
		catch (PDOException $ex) {
			$backtrace = debug_backtrace(false);
			$this->showError($ex, $backtrace, $conn);
		}
	}

	/**
	 * Lesender Datenbank zugriff
	 * @param string @query Query String
	 * @param boolean $single Eindimensionales Array Ausgeben
	 * @return array Ergebnis
	 * @throws SQLException Fehlerhaftes Query
	 */
	public function query($query, $single = false) {
		$backtrace = debug_backtrace(false);
		try {
			$start = microtime();
			$stmt = parent::prepare($query);
			$stmt->execute();			
			$end = microtime();
		}
		catch (PDOException $ex) {
			$this->showError($ex, $backtrace, $query);
		}
		if ($single) {
			$this->setData($end, $start, $query, $backtrace, 1);
			return $stmt->fetch(PDO::FETCH_ASSOC);
		} else {			
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$this->setData($end, $start, $query, $backtrace, count($data));
			return $data;
		}
		return false;
	}
	
	/**
	 * Schreibender Datenbank zugriff
	 * @param string $query Query String
	 * @return integer Anzahl veränderter Einträge
	 */
	public function exec($query) {
		$backtrace = debug_backtrace(false);
		try {
			$start = microtime();
  			$row_count = parent::exec($query);
			$end = microtime();
		} 
		catch (PDOException $ex) {
			$this->showError($ex, $backtrace, $query);
		}
		$this->setData($end, $start, $query, $backtrace, $row_count);
		return $row_count;		
	}
	
	/**
	 * Aktualisieren oder Einfügen
	 * 
	 * Es wird geprüft ob ein Eintrag bereits vorhanden und aktualisiert wird
	 * oder ein neuer Angelegt wird
	 * @param string $table Tabelle
	 * @param array $array Assioziatives Array mit Tabellenspalten und Werten
	 * @param string $where Vergleichsoperatoren
	 */
	public function updateOrInsert($table, $array, $where) {
		$sql = 'SELECT * FROM ' . $table . ' WHERE ' . $where . ' LIMIT 1';
		$data = $this->query($sql);
		if (!empty($data))  {
			$return = $this->update($table, $array, $where);
		} else {
			$return = $this->insert($table, $array);			
		}
		return $return;
	}
	
	/**
	 * Aktualisieren
	 * 
	 * @param string $table Tabelle
	 * @param array $array Assioziatives Array mit Tabellenspalten und Werten
	 * @param string $where Vergleichsoperatoren
	 * @return string Query
	 */
	public function update($table, $array, $where) {
		$sql = 'UPDATE ' . $table . ' SET ';
		$sql .= $this->buildFields($array);
		$sql .= ' WHERE ' . $where;
		$this->exec($sql);
		return $sql;
	}
 
	/**
	 * Einfügen
	 * 
	 * @param string $table Tabelle
	 * @param array $array Assioziatives Array mit Tabellenspalten und Werten	 
	 * @return string Query
	 */
	public function insert($table, $array) {
		$sql = 'INSERT INTO ' . $table . ' SET ';		
		$sql .= $this->buildFields($array);
		$this->exec($sql);
		return $sql;
	}
	
	/**
	 * Array in Query String umwandeln
	 * 
	 * @param array $array Daten
	 * @return string SQL Formatierter String
	 * @throws InvalidArgumentException Wenn kein Array übergeben wurde
	 */
	private function buildFields($array) {
		$fields = '';
		if(!is_array($array)) {
			throw new InvalidArgumentException('Ungültiger Datentyp. Kein Array übergeben!');
		} else {
			foreach ($array as $key => $value) {
				if (is_int($value) || in_array($value, $this->sqlCommands)) {
					$fields .= $key . '=' . $value . ',';	
				}  else {
					$fields .= $key . '="' . $value . '",';	
				}
			}
			$fields = substr($fields, 0, -1);			
		}
		return $fields;
	}
	
	/**
	 * Interne Daten für Debugzwekce aufbereiten
	 * 
	 * @var float $end Zeit Query Ende
	 * @var float $start Zeit Query Start
	 * @var string $query SQL Query
	 * @var array $backtrace Von wo wurde das Statement abgesetzt
	 * @var integer $results Anzahl veränderter Daten
	 */
	private function setData($end, $start, $query, $backtrace, $results) {
		$this->timer += $end - $start;
		$this->counter++;
				
		$this->queries[] = array(
			'counter' => $this->counter,
			'time' => $end - $start,			
			'file' => $backtrace[0]['file'],
			'line' => $backtrace[0]['line'],
			'results' => $results,
			'query' => $query
		);
	}
	
	/**
	 * Fehler Ausgeben
	 */
	private function showError($ex, $backtrace, $query='') {
		$this->errors['error'] = $ex->getMessage();
		$this->errors['file'] = $backtrace[0]['file'];
		$this->errors['line'] = $backtrace[0]['line'];
		$this->errors['query'] = SqlFormatter::format($query);
		if(defined('STDIN')) {
			$this->showCliError();
		} else {
			$this->showHtmlError();
		}
	}
	
	/**
	 * CLI Fehlermeldung ausgeben
	 */
	private function showCliError() {
		echo "Fehler:\t" . $this->errors['error'] . "\n";
		echo "Datei:\t" . $this->errors['file'] . "\n";
		echo "Zeile:\t" . $this->errors['line'] . "\n";
		echo $this->errors['query'];
		die;
	}
	
	/**
	 * HTML Fehlermeldung ausgeben
	 */
	private function showHtmlError() {
		$this->error_page = file_get_contents(__DIR__ . '/sql_error.html');
		foreach ($this->errors as $key => $value) {
			$this->error_page = str_replace('{$' . $key . '}', $value, $this->error_page);
		}
		ob_clean();
		echo $this->error_page;
		die;
	}	
	
	/**
	 * Querys Array Ausgeben
	 * @return array Queries Array
	 */
	public function getQueries() {
		return $this->queries;
	}
	
	/**
	 * Anzahl durchgeführter Queries
	 * @return integer Zähler
	 */
	public function getCounter() {
		return $this->counter;
	}
		
	/**
	 * Laufzeit SQL Abfragen gesamt.
	 * @return float Zeit
	 */
	public function getTimer() {
		return $this->timer;
	}
	
}