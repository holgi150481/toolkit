<?php
/**
 * Authentification Klasse
 * 
 * Benutzer authentifizierung und Verwaltung
 * @package Toolkit
 */

class Authentification {

	/**
	 * Datenbank Objekt
	 * @var object Datenbank
	 */
	private $db;
	
	/** 
	 * Singleton Instance
	 * @var object Instanz
	 */
	private static $instance = null;
	
	/**
	 * Benutzer Id
	 * @var integer Benutzer Id
	 */
	private $user_id = null;
	
	/**
	 * Benutzer Login Name
	 * @var string Login Name
	 */
	private $user;
	
	/**
	 * Benutzer gesperrt?
	 * @var boolean gesperrt?
	 */
    private $locked;
	
	/**
	 * Benutzer Realname
	 * @var string Realname
	 */
    private $name;

	/**
	 * Zu prüfendes Modul
	 * @var string Modul
	 */
	private $module;
	
	/**
	 * Array mit Benutzerrechten
	 * @var array Benutzerrechte
	 */
	private $auth_ids = array();

	/**
	 * Singleton Clone deaktivieren
	 */
	private function __clone() {}

	/**
	 * Singleton aufruf
	 * @return object Instanz
	 */
	public static function getInstance() {
		if (self::$instance == null) {
			self::$instance = new Authentification;	
		}
		return self::$instance;
	}
	
	/**
	 * Konstruktor
	 * 
	 * Datenbankverbindung aufbauen	 
	 */
	private function __construct() {
		$this->db = SQL_PDO::getInstance();
		$this->getModuleData();
	}
	
	/**
	 * Modul Informationen laden
	 */
	private function getModuleData() {
		$sql = 'SELECT id, modul, offen FROM ' . TABLE_SYSTEM_MODULE;
		$this->module = $this->db->query($sql);			
	}
	
	/**
	 * Benutzerrechte aus DB lesen
	 */
	private function getAuthData() {
		if (!empty($this->user_id)) {
			$sql = 'SELECT auth_id FROM ' . TABLE_USER_AUTH . ' WHERE user_id=' . $this->user_id;
			$auth_ids = $this->db->query($sql);
			foreach ($auth_ids as $id) {
				$this->auth_ids[] = $id['auth_id'];
			}
		}
	}

	/**
	 * Prüfung ob zugriff gestattet
	 * @var string $auth Modul
	 * @return boolean Zugriff?
	 */
	public function checkLogin($auth) {		
		foreach ($this->module as $modul) {
			if (strpos($auth, $modul['modul']) !== false) {
			//if ($auth == $modul['modul']) {
				if ($modul['offen'] == 'true') {
					return true;
				} else 	if (in_array($modul['id'], $this->auth_ids)) {
					return true;	
				}
			}
		}
	}
	
	/**
	 * Benutzerdaten aus DB lesen
	 * @param integer $user_id User Id
	 */
	private function fetchUserData($user_id) {
		$sql = 	'SELECT user, locked, CONCAT(vorname, " ", nachname) AS name ' .
				' FROM ' . TABLE_USER_DATA . 
				' WHERE id=' . $user_id;
		$user_data = $this->db->query($sql, true);	
		$this->user_id 	= $user_id;
		$this->user		= $user_data['user'];
		$this->locked	= $user_data['locked'];
		$this->name		= $user_data['name'];
	}

	/**
	 * Zu prüfenden Benutzer setzen
	 * @param integer $user_id User Id
	 */
	public function setUser($user_id) {
		$this->fetchUserData($user_id);
		$this->getAuthData();	
	}

	/**
	 * Anmeldung
	 * @param string $user Benutzername
	 * @param string $pass Passwort
	 */
	public function login($user, $pass) {
		$sql = 	'SELECT id, locked FROM ' . TABLE_USER_DATA . 
				' WHERE user="' . $user . '" AND password="' . sha1($pass) . '" OR password="' . $pass . '"';
		$data = $this->db->query($sql, true);		
		if (!empty($data)) {
			$this->loginSuccess($data, $user);
		} else {
			$this->loginFailed($user, $pass);
		}		
	}
	
	/**
	 * Login Ok
	 * @param array $data Benutzerdaten
	 * @param string $user Benutzername
	 */
	private function loginSuccess($data, $user) {
		if ($data['locked'] == 'true') {
			logger('Login', 'Gesperrter Account' ,'Account: ' . $user , TABLE_LOG_USER);
			$_SESSION['error'] = '<strong>Fehlgeschlagen:</strong>&nbsp;Account gesperrt';			
		} else {
			$this->fetchUserData($data['id']);
			$_SESSION['user'] = $this->user_id;
			logger('Login', 'Erfolgreich' ,'Account: ' . $user , TABLE_LOG_USER);
			$_SESSION['success'] = '<strong>Login:</strong>&nbsp;Erfolgreich';
		}
	}
	
	/**
	 * Login fehlgeschlagen
	 * @param array $data Benutzerdaten
	 * @param string $user Benutzername
	 */
	private function loginFailed($user, $pass) {
		logger('Login', 'Fehlgeschlagen' ,'ID: ' . $user . ' PW: ' . $pass, TABLE_LOG_USER);
		unset($_SESSION['user']);
		$_SESSION['error'] = '<strong>Login:</strong>&nbsp;Fehlgeschlagen';
	}

	/** 
	 * Abmeldung
	 */
	public function logout() {
		unset($_SESSION['user']);		
		$_SESSION['info'] = '<strong>Login:</strong>&nbsp;Abgemeldet';
	}
	
	/** 
	 * User ID abrufen
	 * @return integer User Id
	 */
	public function getUserId() {
		return $this->user_id;
	}
	
	/**
	 * Bentzer Namen abrufen
	 * @return string Username
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * Realname abrufen
	 * @return string Realname
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Prüfung ob Benutzer angemeldet ist
	 * @return boolean Angemeldet?
	 */
	public function isLoggedIn() {
		if (!empty($this->user_id)) {
			return true;
		}
	}
}