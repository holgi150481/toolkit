<?php   

require_once "Net/GeoIP.php";


 /**
 * Tracking von IP Adressen
 * 
 * Zum Tracken von Seiten Besuchern werden IP Adressen
 * u.a. in GeoDaten, Whois und weiteren Daten Ausgewertet
 * @package Toolkit
 */

class Tracking {

	/**
	 * Datenbank Verbindung	 
	 * @var object
	 */
	private $db;
	
	/**
	 * Datenbank Tabelle
	 * @var string
	 */
	private $table = TABLE_TRACKING;
	
	/**
	 * Daten Array
	 * @var array
	 */
	private $data = array();

	/**
	 * Server Daten
	 * @var string
	 */
	private $serverData;
	
	/**
	 * Manuelles Info Feld
	 * @var string
	 */
	private $info;
	
	/**
	 * Geo Daten der IP Adresse
	 * @var array
	 */
	private $geoData;	

	/** 
	 * Internet Provider
	 * @var string
	 */
	private $provider;

	const PATTERN_PROVIDER 	= "/(descr:\s*)(.*?)(descr|country|static|remarks|netname)/";	


	/**
	 * Konstruktor
	 * 
	 * Datenbank Verbindung aufbauen
	 */
	public function __construct() {
		$this->db = SQL_PDO::getInstance();
	}

	/**
	 * Ip Adresse Setzen
	 * @param string $ip IP Adresse
	 * @return object This
	 */
	public function setIp($ip) {
			return $this;
	}

	/** 
	 * PHP $_SERVER Array übergeben.
	 * @param array $serverData $_SERVER
	 * @return object This
	 */
	public function setServerData($serverData) {
		$this->serverData = $serverData;		
		return $this;
	}

	/**
	 * Information String setzen
	 * @param string $info Info String
	 * @return object This
	 */
	public function setInfo($info) {
		$this->info = $info;
		return $this;
	}

	/**
	 * Seitenzähler auslesen
	 * @return integer Zähler
	 */
	public function getCounter() {
		$sql = 'SELECT COUNT(id) AS counter FROM ' .$this->table;
		$counter = $this->db->query($sql, true);
		return $counter['counter'];
	}

	/**
	 * Tracking ausführen
	 * @return object This
	 */
	public function track() {
		$this->fetchGeoData();
		$this->fetchRipe();		
		$this->buildArray();		
		$this->db->updateOrInsert($this->table, $this->data, 'ip="' .$this->serverData['REMOTE_ADDR'] . '" AND datum <> NOW()');
		return $this;
	}
	
	/** 
	 * Geo Daten abfragen
	 */
	private function fetchGeoData() {
		$geoLocation = Net_GeoIP::getInstance("/var/www/include/GeoLiteCity.dat");
		$this->geoData = $geoLocation->lookupLocation($this->serverData['REMOTE_ADDR'])->getData();
	}

	/**
	 * Ripe Whois durchführen
	 */
	private function fetchRipe() {
		if (!empty($this->serverData['REMOTE_ADDR'])) {
			$ripe = shell_exec('whois ' . $this->serverData['REMOTE_ADDR']);
			$ripe = str_replace("\n", '', $ripe);
			preg_match(self::PATTERN_PROVIDER, $ripe, $match);
			$this->provider = $match[2];
		} else {
			$this->provider = (string)$match;
		}
	}

	/**
	 * Array zusammenbauen
	 */
	private function buildArray() {
		$this->data = array(
			'ip' 			=> $this->serverData['REMOTE_ADDR'],
			'datum' 		=> date('Y-m-d'),			
			'zeit' 			=> date('H:m:s'),
			'info'			=> $this->info,
			'country_code' 	=> $this->geoData['countryCode'],
			'country_name' 	=> $this->geoData['countryName'],
			'city' 			=> $this->geoData['city'],	
			'latitude' 		=> $this->geoData['latitude'],
			'longitude' 	=> $this->geoData['longitude'],
			'user_agent' 	=> (!empty($this->serverData['HTTP_USER_AGENT']) ? $this->serverData['HTTP_USER_AGENT'] : ''),
			'host_name'		=> gethostbyaddr($this->serverData['REMOTE_ADDR']),
			'provider'		=> $this->provider
		);
	}

}