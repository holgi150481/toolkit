<?php
/**
 * Speichern von Key/Value paaren. 
 * 
 * Es werden Key/Value Paare in einer Datenbank gespeichert.  
 * @package Toolkit
 */
 
class Settings {
	
	/**
	 * Beinhaltet aktuelle Key/Value Paare.
	 * @var array Key/Value Paare
	 */
	private $settings = array();
	
	/**
	 * Beinhaltet alte Key/Value Paare um Speichern/Aktualisieren/Löschen zu prüfen.
	 * @var array Key/Value Paare
	 */
	
	private $settings_old = array();
	
	/**
	 * Datenbank Object
	 * @var object DB Objekt
	 */
	private $db;	
	
	/**
	 * Konstruktor
	 * 
	 * Daten aus DB lesen.
	 */
	public function __construct() {
		$this->db = SQL_PDO::getInstance();		
		$sql = 'SELECT * FROM ' . TABLE_SETTINGS;
		$data = $this->db->query($sql);		
		foreach ($data as $key => $value) {
			$this->settings[$value['set_group']][$value['set_key']] = $value['set_value'];
			$this->settings_old[$value['set_group']][$value['set_key']] = $value['set_value'];
		}
	}
	
	/**
	 * Destruktor
	 * 
	 * Daten in DB schreiben oder aktualisieren.
	 */
	public function __destruct() {
		foreach ($this->settings as $group => $value) {
			foreach ($this->settings[$group] as $key => $val) {
				if (array_key_exists($key, $this->settings_old[$group])) {
					if ($val != $this->settings_old[$group][$key]) {
						$this->update($group, $key, $val);
					}
					if (empty($val)) {
						$this->delete($group, $key);
					}  
				} else {					
					$this->insert($group, $key, $val);
				}
			}
		}
	}
	
	/**
	 * Daten setzen.
	 * @param string $group Gruppe
	 * @param string $key Schlüssel
	 * @param array|string $value Einstellungen
	 */
	public function set($group, $key, $value) {
		$this->settings[$group][$key] = $this->checkArray($value);
	}
	
	/**
	 * Daten auslesen.
	 * @param string $group Gruppe
	 * @param string $key optionaler Schlüssel
	 * @return string|array Werte der Schlüssels
	 */
	public function get($group, $key="") {
		if (isset($this->settings[$group])) {		
			if (!empty($key)) {
				return (!empty($this->settings[$group][$key]) ? $this->checkJson($this->settings[$group][$key]) : '');
			} else {
				foreach ($this->settings[$group] as $key => $value) {
					$ret[$key] = $this->checkJson($value);				
				}
				return $ret;	
			}			
		}
		return false;
	}
	
	/**
	 * Eintrag entfernen.
	 * @param string $group Gruppe
	 * @param string $key Schlüssel
	 */
	public function remove($group, $key) {
		$this->delete($group, $key);
	}
	
	/**
	 * JSON prüfung 
	 *
	 * Prüfen ob Eintrag ein JSON String ist und in Array konvertieren.
	 * @param string Eintrag
	 */
	private function checkJson($value) {
		$data = @json_decode($value);
		if (!$data) {
    		return $value;
		} else {
    		return $data;
		}
	}
	
	/**
	 * Array prüfung
	 *
	 * Prüfung ob String ein Array ist, dann in JSON Konvertieren.
	 * @param string|array Eintrag
	 */
	private function checkArray($value) {		
		if (is_array($value)) {
    		return json_encode($value);
		} else {
    		return $value;
		}
	}
	
	/**
	 * DB Eintrag schreiben
	 * @param string $group Gruppe
	 * @param string $key Schlüssel
	 * @param string $value Wert
	 */
	private function insert($group, $key, $value) {		
		$sql =  'INSERT INTO ' . TABLE_SETTINGS . ' SET ' . 
				' set_group="' . $group . '"' .
				',set_key="' . $key  . '"' .
				',set_value="' . addslashes($value) . '"';		
		$this->db->exec($sql);
	}

	/**
	 * DB Eintrag aktualisieren
	 * @param string $group Gruppe
	 * @param string $key Schlüssel
	 * @param string $value Wert
	 */
	private function update($group, $key, $value) {
		$sql =  'UPDATE ' . TABLE_SETTINGS . ' SET ' . 
				' set_group="' . $group . '"' .
				',set_key="' . $key  . '"' .
				',set_value="' . addslashes($value) . '"' .
				' WHERE set_group="' . $group . '" AND set_key="' . $key . '"';
		$this->db->exec($sql);
	}
	
	/**
	 * DB Eintrag löschen
	 * @param string $group Gruppe
	 * @param string $key Schlüssel	 
	 */
	private function delete($group, $key) {
		$sql =  'DELETE FROM ' . TABLE_SETTINGS .  
				' WHERE set_group="' . $group . '" AND set_key="' . $key . '"';
		$this->db->exec($sql);
	}
	
}